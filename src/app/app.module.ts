import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { RootpagePage } from '../pages/rootpage/rootpage';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { ProfileContactPage } from '../pages/profileContact/profile';
import { ModalContentPage } from '../pages/modal/ModalContentAttachmentPage';
import { ModalContentPostPage } from '../pages/modalAddPost/ModalContentPostPage';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { CommunicationPage } from '../pages/communication/communication';
import { TabsPage } from '../pages/tabs/tabs';
import {SearchPage} from "../pages/search/search";
import {SearchResultPage} from "../pages/search-result/search-result";
import {CookiePage} from "../pages/cookie/cookie";
import {PrivacyPage} from "../pages/privacy/privacy";
import {TermsPage} from "../pages/terms/terms";
import {MessagePage} from "../pages/message/message";
import {MeetingPage} from "../pages/meeting/meeting"
import {SendMessagePage} from "../pages/send-message/send-message";
import {ConfirmationPage} from "../pages/confirmation/confirmation";
import {ConfirmDeletePage} from "../pages/confirm-delete/confirm-delete";
import {ConfirmPublishingPage} from "../pages/confirm-publishing/confirm-publishing";
import {ResetPasswordPage} from "../pages/reset-password/reset-password";
import {ResetPasswordTokenPage} from "../pages/reset-password-token/reset-password-token";
import {Storage} from '@ionic/storage';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    MyApp,
    RootpagePage,
    RegisterPage,
    ContactPage,
    HomePage,
    LoginPage,
    CommunicationPage,
    ProfilePage,
    ProfileContactPage,
    SearchPage,
    SearchResultPage,
    CookiePage,
    PrivacyPage,
    TermsPage,
    ModalContentPage,
    MessagePage,
    MeetingPage,
    SendMessagePage,
    ModalContentPostPage,
    ResetPasswordPage,
    ResetPasswordTokenPage,
    ConfirmationPage,
    ConfirmDeletePage,
    ConfirmPublishingPage,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    MomentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RootpagePage,
    RegisterPage,
    ContactPage,
    HomePage,
    LoginPage,
    CommunicationPage,
    ProfilePage,
    ProfileContactPage,
    SearchPage,
    SearchResultPage,
    CookiePage,
    PrivacyPage,
    TermsPage,
    ModalContentPage,
    MessagePage,
    MeetingPage,
    SendMessagePage,
    ModalContentPostPage,
    ResetPasswordPage,
    ResetPasswordTokenPage,
    ConfirmationPage,
    ConfirmDeletePage,
    ConfirmPublishingPage,
    TabsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},Storage]
})
export class AppModule {}
