import { Component } from '@angular/core';
import { NavController, NavParams , ViewController,AlertController} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {AppVersion} from "ionic-native";

/*
  Generated class for the Rootpage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-rootpage',
  templateUrl: 'rootpage.html'
})
export class RootpagePage {
  private versionNumber: string;
  spinner = false
  constructor(public nav: NavController,private http:Http, public navParams: NavParams,private viewCtrl: ViewController,public alertCtrl: AlertController) {
    this.http = http;
  }


  ionViewWillEnter() {
    console.log('root page')
    let th = this;

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let token=localStorage.getItem("token");
    //let s = '1.0'
     AppVersion.getVersionNumber().then((s) => {
        console.log('ver',s);
        th.versionNumber = s;


        th.http.get('https://api.denrole.com/api/check_version?version='+s, headers).map(res => res.json()).subscribe(
          data => {
            let response = data;
            console.log(response);
            if (data.code == 74){
              if (!data.data.isCurrent){
                th.spinner= true;
                let alert = th.alertCtrl.create({
                  title: 'Alert',
                  subTitle: 'Please update your app',
                  buttons: ['OK']
                });
                alert.present();
              }
              else {
                if (token != null){
                  th.http.get('https://api.denrole.com/api/posts?token='+localStorage.getItem("token"), headers).map(res => res.json()).subscribe(
                    data => {
                      let response = data;
                      console.log(response);
                      if (data.code == 74){
                        th.nav.push(TabsPage).then(() => {
                          // first we find the index of the current view controller:
                          const index = th.viewCtrl.index;
                          // then we remove it from the navigation stack
                          th.nav.remove(index);
                        });
                      }
                      else {
                        th.nav.push(LoginPage).then(() => {
                          // first we find the index of the current view controller:
                          const index = th.viewCtrl.index;
                          // then we remove it from the navigation stack
                          th.nav.remove(index);
                        });
                        let alert = th.alertCtrl.create({
                          title: 'Alert',
                          subTitle: data.message,
                          buttons: ['OK']
                        });
                        alert.present();
                      }
                    },
                    error => {
                      console.log("ERROR: ", error);
                      th.nav.push(LoginPage).then(() => {
                        // first we find the index of the current view controller:
                        const index = th.viewCtrl.index;
                        // then we remove it from the navigation stack
                        th.nav.remove(index);
                      });
                    }
                  );
                }
                else {
                  th.nav.push(LoginPage).then(() => {
                    // first we find the index of the current view controller:
                    const index = th.viewCtrl.index;
                    // then we remove it from the navigation stack
                    th.nav.remove(index);
                  });
                }
              }
            }
            else {
              let alert = th.alertCtrl.create({
                title: 'Alert',
                subTitle: data.message,
                buttons: ['OK']
              });
              alert.present();
            }

          },
          error => {
            console.log("ERROR: ", error);
            let alert = this.alertCtrl.create({
              title: 'Connection issue',
              message: 'Please check your internet connection then click OK',
              buttons: [
                {
                  text: 'OK',
                  role: 'cancel',
                  handler: () => {

                    th.ionViewWillEnter();
                  }
                }
              ]
            });
            alert.present();
            /*let alert1 = th.alertCtrl.create({
              title: 'Alert',
              subTitle: '',
              buttons: ['OK']
            });
            alert1.present();
            alert1.dismiss();*/
/*            th.nav.push(LoginPage).then(() => {
              // first we find the index of the current view controller:
              const index = th.viewCtrl.index;
              // then we remove it from the navigation stack
              th.nav.remove(index);
            });*/
          }
        );

     })




  }
}
