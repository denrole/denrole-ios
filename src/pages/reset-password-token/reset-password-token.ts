import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';

import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {LoginPage} from "../login/login";
/*
  Generated class for the ResetPasswordToken page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-reset-password-token',
  templateUrl: 'reset-password-token.html'
})
export class ResetPasswordTokenPage {
  user={
    email: "",
    password: "",
    token: ""
  };
  constructor(public navCtrl: NavController, public navParams: NavParams,private http:Http, public alertCtrl: AlertController) {
    this.http = http;
  }

  onReset(){


    let headers = new Headers({ 'Content-Type': 'application/json' });
    let url = 'https://api.denrole.com/api/reset';
    let body = this.user;
    let response;
    console.log(body);
    this.http.post(url, body, headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            let alert = this.alertCtrl.create({
              title: 'Success',
              subTitle: data.message,
              buttons: ['OK']
            });
            alert.present();
            this.navCtrl.push(LoginPage)

          }
          else {
            let alertf = this.alertCtrl.create({
              title: 'Failure',
              subTitle: data.message,
              buttons: ['OK']
            });
            alertf.present();

          }
        },
        error => {
          console.log("ERROR: ", error);
          let alertf = this.alertCtrl.create({
            title: 'Failure',
            subTitle:'Code invalid',
            buttons: ['OK']
          });
          alertf.present();
        }
    );

    // console.log(this.http.post(url, body, headers).map(res => res.json()));

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordTokenPage');
  }

}
