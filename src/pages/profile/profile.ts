import {Component, ElementRef, ViewChild} from '@angular/core';
import {Headers,Http} from '@angular/http';
import { NavController , NavParams,ModalController,ViewController,AlertController} from 'ionic-angular';
import { ModalContentPage } from '../modal/ModalContentAttachmentPage';
import {File} from 'ionic-native';
import { ModalContentPostPage } from '../modalAddPost/ModalContentPostPage';
import {ConfirmPublishingPage} from "../confirm-publishing/confirm-publishing";
import {ConfirmDeletePage} from "../confirm-delete/confirm-delete";
import {TabsPage} from "../tabs/tabs";
import { App } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  @ViewChild('fileInput') fileInput:ElementRef;
    row ;
    user={
    username: '',
    token: localStorage.getItem("token"),
    profile_picture:'',
    is_published: 0
    };
    filesToUpload: Array<File>;
    roleArray=[];
    workHArray=[];
    typeArray=[];
    storageDirectory: string = '';
    attachments:Array<Object>;
    headers = new Headers({ 'Content-Type': 'application/json' });
    url = 'https://api.denrole.com/api/';
    powerOff = new TabsPage( this.navParams,  this.nav,this.viewCtrl,this.app,this.Alert)

  constructor(  public nav: NavController,public navParams: NavParams,private viewCtrl: ViewController,
              private http:Http, public modalCtrl: ModalController,public Alert: AlertController, private app : App,
  ) {
        this.http=http;

  }
    ionViewWillEnter() {

        let body = this.user;
        let response;
        let varArray =[] ;
        this.row = [];
        console.log(body);
        this.http.get(this.url+'profile?token='+this.user.token, this.headers).map(res => res.json()).subscribe(
            data => {
                response = data;
                console.log(response);
                if (data.code == 74){

                    this.user.username = data.data.user.username;
                    this.user.profile_picture = 'https://api.denrole.com/uploads/profile_pictures/'+data.data.user.profile_picture;
                    this.user.is_published = data.data.user.is_published;
                    this.attachments = data.data.attachments;
                    console.log(this.attachments);
                    for (let i = 0; i < this.attachments.length; ){

                        for(let j =0 ; j < 3 && i < this.attachments.length; j ++){
                            varArray.push(this.attachments[i]);
                            i++;
                        }
                        this.row.push(varArray);
                        varArray = []
                    }
                    console.log(this.row)

                }
                else {

                }
            },
            error => {
                console.log("ERROR: ", error)
            }
        );
        this.http.get(this.url+'posts/form_elemens?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
            data => {
                response = data;
                console.log(response);
                if (data.code == 74){
                    if(data.data.roles){
                        this.roleArray = data.data.roles;
                    }
                    if (data.data.work_hours){
                        this.workHArray = data.data.work_hours;
                    }
                    if (data.data.types){
                        this.typeArray= data.data.types;
                    }
                }
                else {

                }
            },
            error => {
                console.log("ERROR: ", error)
            }
        );


    }

    upload() {
        let url = 'https://api.denrole.com/api/profile/edit_picture?token='+localStorage.getItem("token");
        this.makeFileRequest(url, [], this.filesToUpload).then((result) => {

            this.ionViewWillEnter();
        }, (error) => {
            console.error(error);
        });
    }
    fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>> fileInput.target.files;
        this.upload();
    }
    makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
        return new Promise((resolve, reject) => {
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();
            for(let i = 0; i < files.length; i++) {
                formData.append("profile_picture", files[i]);
            }
            var profile = this
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                        profile.ionViewWillEnter();
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            console.log(formData)
            xhr.send(formData);
        });
    }
    pubishProfile(){
        this.nav.push(ConfirmPublishingPage);

    }

    openModalAttachment() {
        let modal = this.modalCtrl.create(ModalContentPage);
        modal.present();
    }
    openModalPost() {
        let modal = this.modalCtrl.create(ModalContentPostPage, {
            roleArray: this.roleArray,
            workHArray: this.workHArray,
            typeArray: this.typeArray
        });
        modal.present();
    }

    deleteAttachment(id){
        this.nav.push(ConfirmDeletePage,{
            id : id
        });
    }
    logout(){
      let alert = this.Alert.create({
        title: 'Confirm logout',
        message: 'Do you want to logout',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'logout',
            handler: () => {
              console.log('logout clicked');
            }
          }
        ]
      });
      alert.present();
    }


}
