import { Component } from '@angular/core';
import {Headers,Http} from '@angular/http';
import {NavController, NavParams, ViewController, AlertController} from 'ionic-angular';
import {MessagePage} from "../message/message";
import {TabsPage} from "../tabs/tabs";
import { App } from 'ionic-angular';
@Component({
  selector: 'page-communication',
  templateUrl: 'communication.html'
})
export class CommunicationPage {
  message:Array<Object>;
  sender:'';
  picture_sender:'';
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  id = localStorage.getItem("id");

    powerOff = new TabsPage( this.navParams,  this.nav,this.viewCtrl,this.app,this.Alert)

  constructor(private http:Http,public nav: NavController, public navParams: NavParams, private viewCtrl: ViewController,private app:App,public Alert: AlertController) {
    this.http=http;

  }
  ionViewWillEnter() {
    let response;
    let messagearray=[];
    this.http.get(this.url+'messages?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            console.log(data)
              if(data.data.messages) {
                for(var i = 0; i < data.data.messages.length; i++) {
                  messagearray[i]=( data.data.messages[i]);
                }
                this.message=messagearray;
              }
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );

  }
  onClickMessage(message_id){
      this.nav.push(MessagePage,{
          message_id:message_id
      })
  }
}
