import { Component } from '@angular/core';
import {Headers,Http} from '@angular/http';
import { NavController , AlertController,NavParams,ViewController} from 'ionic-angular';
import { ProfileContactPage } from '../profileContact/profile';
import {ProfilePage} from "../profile/profile";
import {TabsPage} from "../tabs/tabs";
import { App } from 'ionic-angular';
import {MomentModule} from 'angular2-moment/module';
import * as moment from 'moment';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  boardposts :Array<Object>;
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  user = {
      id: localStorage.getItem("id")
};
  formattedLocalDate;
  powerOff = new TabsPage( this.navParams,  this.nav,this.viewCtrl,this.app,this.alertCtrl)

  constructor(private http:Http,public alertCtrl: AlertController, public navParams: NavParams, public nav: NavController,private viewCtrl: ViewController,private app : App) {
    this.http=http;

  }

  ionViewWillEnter() {
    console.log()
    var x = new Date();
    var currentTimeZoneOffsetInHours = x.getTimezoneOffset() / 60;

    var d = new Date(); // or whatever date you have
    var tzName = d.toLocaleString('en', {timeZoneName:'short'}).split(' ').pop();
    console.log(tzName)
    d.setTime( d.getTime() - d.getTimezoneOffset()/ 60);
    console.log(d.getTime())

    var now = new Date('11/07/2017');
    var utc_timestamp = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate() ,
      now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());

    console.log(utc_timestamp)
    console.log(new Date(utc_timestamp))
    var zone = new Date().toLocaleTimeString('en-us',{timeZoneName:'short'}).split(' ')[2]
    console.log(zone)
    console.log(this.user.id)
    let response;
    let boardposts2=[];
    this.http.get(this.url+'posts?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){

            if (data.data.posts){


              for (let j = 0; j < data.data.posts.length ; j++){
                //data.data.posts[j].body = data.data.posts[j].body.replace(/&?<?>/g, '');
                data.data.posts[j].body = data.data.posts[j].body.replace(/\r?\n/g, '<br />');
                data.data.posts[j].body = this.strip_tags(data.data.posts[j].body,'<br>')
                console.log((data.data.posts[j]))

                var utcDate = moment.utc((data.data.posts[j]).updated_at.date)
                var localDate = utcDate.local()
                this.formattedLocalDate = localDate.format("DD/MM/YYYY HH:mm")

               data.data.posts[j].updated_at.date = this.formattedLocalDate
                console.log((data.data.posts[j]).updated_at.date )
                boardposts2 = data.data.posts;

                this.boardposts= boardposts2;

              }
            }
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );

    var now = new Date();
    var utc_timestamp = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate() ,
      now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());

    console.log(new Date(utc_timestamp))
  }
  OnClickAvatar(id,boolean){
      console.log(id)
      if (id == this.user.id){
          this.nav.push(ProfilePage);
      } else
      {
          localStorage.setItem("idOtherUser",id);
          localStorage.setItem("isfriend",boolean);
          this.nav.push(ProfileContactPage);
      }

  }
  sendAdd(id){
      console.log("add friend");
    let response;
    this.http.get(this.url+'contacts/'+id+'/send_request?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
              let alert = this.alertCtrl.create({
                  title: 'Success',
                  subTitle: data.message,
                  buttons: ['OK']
              });
              alert.present();
            this.ionViewWillEnter()
          }
          else {
              let alert = this.alertCtrl.create({
                  title: 'Alert',
                  subTitle: "Invitation already sent to this user",
                  buttons: ['OK']
              });
              alert.present();
          }
        },
        error => {
          console.log("ERROR: ", error  );

            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: "Invitation already sent to this user",
                buttons: ['OK']
            });
            alert.present();
        }
    );
  }



  strip_tags(input, allowed) {


    allowed = (((allowed || '') + '')
      .toLowerCase()
      .match(/<[a-z][a-z0-9]*>/g) || [])
      .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
      commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
      .replace(tags, function($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
      });
  }


  deletePost(id){
    let response;
    this.http.get(this.url+'posts/'+id+'/remove?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
      data => {
        response = data;
        console.log(response);
        if (data.code == 74){
          let alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: data.message,
            buttons: ['OK']
          });
          alert.present();
          this.ionViewWillEnter()
        }
        else {
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: "Invitation already sent to this user",
            buttons: ['OK']
          });
          alert.present();
        }
      },
      error => {
        console.log("ERROR: ", error  );

        let alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: "Invitation already sent to this user",
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
}
