import { Component } from '@angular/core';
import {Headers,Http} from '@angular/http';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import {ProfilePage} from "../profile/profile";

/*
  Generated class for the ConfirmPublishing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-confirm-publishing',
  templateUrl: 'confirm-publishing.html'
})
export class ConfirmPublishingPage {

  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  user={
    username: '',
    token: localStorage.getItem("token"),
    profile_picture:'',
    is_published: 0
  };

  constructor(public navCtrl: NavController,  private http:Http, public navParams: NavParams,public Alert: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPublishingPage');
  }
  publishProfile(){
    let response;
    this.http.get(this.url+'profile/publish_profile?token='+this.user.token, this.headers).map(res => res.json()).subscribe(
        data => {
            response = data;
            console.log(response);
            if (data.code == 74){
                console.log(data.data.user.is_published);

                let alert = this.Alert.create({
                    title: 'Success',
                    subTitle: data.message,
                    buttons: ['OK']
                });
                alert.present();
                this.navCtrl.push(ProfilePage);
            }
            else {
                let alert = this.Alert.create({
                    title: 'Warning',
                    subTitle: data.message,
                    buttons: ['OK']
                });
                alert.present();
                this.navCtrl.push(ProfilePage);
            }
        },
        error => {
            console.log("ERROR: ", error)
            let alert = this.Alert.create({
                title: 'Alert',
                subTitle: "Your profile already Published",
                buttons: ['OK']
            });
            alert.present();
          this.navCtrl.push(ProfilePage);
        }
    );
  }
  cancelpublishProfile(){
    this.navCtrl.push(ProfilePage);
  }
}
