import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { PrivacyPage } from '../privacy/privacy';
import { TermsPage } from '../terms/terms';
import { CookiePage } from '../cookie/cookie';

import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {ResetPasswordTokenPage} from "../reset-password-token/reset-password-token";

/*
  Generated class for the ResetPassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html'
})
export class ResetPasswordPage {
  user={
    email: ""
  };
  constructor(public nav: NavController, public navParams: NavParams,private http:Http, public alertCtrl: AlertController) {
    this.http = http;
  }


  onForgot(){


    let headers = new Headers({ 'Content-Type': 'application/json' });
    let url = 'https://api.denrole.com/api/forgot';
    let body = this.user;
    let response;
    console.log(body);
    this.http.post(url, body, headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            let alert = this.alertCtrl.create({
              title: 'Success',
              subTitle: data.message,
              buttons: ['OK']
            });
            alert.present();
            this.nav.push(ResetPasswordTokenPage)

          }
          else {
            let alert = this.alertCtrl.create({
              title: 'Alert',
              subTitle: data.data.email,
              buttons: ['OK']
            });
            alert.present();
          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );

    // console.log(this.http.post(url, body, headers).map(res => res.json()));

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }
  openTermsPage(){
    this.nav.push(TermsPage);
  }
  openPrivacyPage(){
    this.nav.push(PrivacyPage);
  }
  openCookiePage(){
    this.nav.push(CookiePage);
  }
}
