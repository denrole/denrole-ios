import { Component, PipeTransform } from '@angular/core';
import {NavController, NavParams, AlertController, ViewController} from 'ionic-angular';
import {Headers,Http} from '@angular/http';
import {ProfileContactPage} from "../profileContact/profile";
import { DatePipe } from '@angular/common';

/*
  Generated class for the Meeting page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-meeting',
  templateUrl: 'meeting.html'
})
export class MeetingPage {
  date = new Date().toISOString();
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  msg = "Meeting request for ";
  constructor(private http:Http,public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,private viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log(this.navParams.get('idContact'));
  }
  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'dd MMM yyyy');
    return value;
  }
  send(){
    console.log(this.transform(this.date));
    console.log(localStorage.getItem("token"))
    let id = localStorage.getItem("idOtherUser");
    console.log(id);
    let response;
    if (this.msg) {
      let body = {
        "body" : this.msg + this.transform(this.date)
      }
      console.log(this.url + 'messages/contact/' +id+ '/send?token=' + localStorage.getItem("token"))
      this.http.post(this.url + 'messages/contact/' +id+ '/send?token=' + localStorage.getItem("token"), body, this.headers).map(res => res.json()).subscribe(
          data => {
            response = data;
            console.log(response);
            if (data.code == 74) {

              this.msg='';
              this.navCtrl.push(ProfileContactPage).then(() => {
                //   // first we find the index of the current view controller:
                   const index = this.viewCtrl.index;
                //   // then we remove it from the navigation stack
                   this.navCtrl.remove(index);;
              })
            }
            else {
              if (data.code == 41){
                let alert = this.alertCtrl.create({
                  title: 'Alert',
                  subTitle: data.message,
                  buttons: ['OK']
                });
                alert.present();
                this.navCtrl.push(ProfileContactPage).then(() => {
                  //   // first we find the index of the current view controller:
                  const index = this.viewCtrl.index;
                  //   // then we remove it from the navigation stack
                  this.navCtrl.remove(index);;
                });
              }
            }
          },
          error => {
            console.log("ERROR: ", error)
          }
      );
    }

  }

}
