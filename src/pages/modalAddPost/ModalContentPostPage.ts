import { Component } from '@angular/core';
import { NavController , NavParams,ViewController,AlertController} from 'ionic-angular';
import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  templateUrl: 'modalPost.html'
})
export class ModalContentPostPage {
  Userpost={
    role:'',
    work_hours:'',
    type:'',
    body:'',
  };
  roleArray=[];
  workHArray=[];
  typeArray=[];
  draftArray=[];
    headers = new Headers({ 'Content-Type': 'application/json' });
    url = 'https://api.denrole.com/api/';

  constructor(
      public nav: NavController,
      private http:Http,
      public viewCtrl: ViewController,
      private navParams: NavParams,
      public alertCtrl: AlertController
  ) {
      this.http=http;
      this.nav = nav;


  }

    ionViewWillEnter(){
        let role= localStorage.getItem("userDraftRole");
        let type= localStorage.getItem("userDraftType");
        let work = localStorage.getItem("userDraftWork");
        let description = localStorage.getItem("userDraftDesc");
        this.Userpost.role = role;
        this.Userpost.work_hours = work;
        this.Userpost.type = type;
        this.Userpost.body = description;
        this.roleArray=this.navParams.get("roleArray");
        this.workHArray=this.navParams.get("workHArray");
        this.typeArray=this.navParams.get("typeArray");
      //
      // let response;
      //   this.http.get(this.url+'posts/form_elemens?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
      //       data => {
      //           response = data;
      //           console.log(response);
      //           if (data.code == 74){
      //               if(data.data.roles){
      //                   this.roleArray = data.data.roles;
      //               }
      //               if (data.data.work_hours){
      //                   this.workHArray = data.data.work_hours;
      //               }
      //               if (data.data.types){
      //                   this.typeArray= data.data.types;
      //               }
      //           }
      //           else {
      //
      //           }
      //       },
      //       error => {
      //           console.log("ERROR: ", error)
      //       }
      //   );

    }

  strip_tags(input, allowed) {


    allowed = (((allowed || '') + '')
      .toLowerCase()
      .match(/<[a-z][a-z0-9]*>/g) || [])
      .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
      commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
      .replace(tags, function($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
      });
  }

  onPost(){

      let url = 'https://api.denrole.com/api/posts/create/publish?token='+localStorage.getItem("token");
      let body ={
        role:this.Userpost.role,
        work_hours:this.Userpost.work_hours,
        type:this.Userpost.type,
        body:this.Userpost.body,
      };


      body.body = body.body.replace(/\r?\n/g, '<br />');
      body.body = this.strip_tags(body.body,'<br>');
      let response;
      let headers = new Headers({ 'Content-Type': 'application/json' });
      console.log(body);
        this.http.post(url, body, headers).map(res => res.json()).subscribe(

            data => {
                response = data;
                console.log(response);
                if (data.code == 74){
                    let alert = this.alertCtrl.create({
                        title: 'Success',
                        subTitle: data.message,
                        buttons: ['OK']
                    });
                    alert.present();
                    this.dismiss();
                    localStorage.removeItem("userDraftRole");
                    localStorage.removeItem("userDraftType");
                    localStorage.removeItem("userDraftWork");
                    localStorage.removeItem("userDraftDesc");
                }
                else {
                    let alert = this.alertCtrl.create({
                        title: 'Alert',
                        subTitle: data.message,
                        buttons: ['OK']
                    });
                    alert.present();
                }
            },
            error => {
                console.log("ERROR: ", error)
            }
        );
  }
  onSaveDraft(){
    console.log(this.Userpost)
    console.log(this.Userpost.body.replace(/\r?\n/g, '<br />'))
      localStorage.setItem("userDraftRole", this.Userpost.role);
      localStorage.setItem("userDraftType", this.Userpost.type);
      localStorage.setItem("userDraftWork", this.Userpost.work_hours);
      if (this.Userpost.body == null) {
          localStorage.setItem("userDraftDesc", " ");
      }else {
          localStorage.setItem("userDraftDesc", this.Userpost.body);
      }
      let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Your Post is successfully saved',
          buttons: ['OK']
      });
      alert.present();
      this.dismiss();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
// .input-cover {
//     position: static;
// }
