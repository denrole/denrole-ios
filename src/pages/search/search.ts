import { Component } from '@angular/core';

import {NavController, NavParams, ViewController, AlertController} from 'ionic-angular';
import {SearchResultPage} from "../search-result/search-result";
import {TabsPage} from "../tabs/tabs";
import { App } from 'ionic-angular';
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  searchItem= {
    name:'',
    role:'',
    location:''
}
  powerOff = new TabsPage( this.navParams,  this.navCtrl,this.viewCtrl,this.app,this.alertCtrl)
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,private app : App,private alertCtrl : AlertController) {

  }
  onclickSearch(){
    let searchItem= this.searchItem;
    console.log(searchItem)
      this.navCtrl.push(SearchResultPage,{
        searchItem : searchItem
      })
  }

}
