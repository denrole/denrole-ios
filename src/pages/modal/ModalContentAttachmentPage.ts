import { Component } from '@angular/core';
import { App, NavController, Tabs , NavParams,ModalController,ViewController,AlertController} from 'ionic-angular';
import {Http} from '@angular/http';
import { TabsPage } from '../tabs/tabs';
import {File,} from 'ionic-native';
import 'rxjs/add/operator/map';
import {NgZone} from '@angular/core';
import {ProfilePage} from "../profile/profile";

@Component({
  templateUrl: 'modal.html'
})
export class ModalContentPage {
  att={
    attfile:'',
    name:'',
    type:''
  };
    tab:Tabs;

    url = 'https://api.denrole.com/api/profile/add_attachment?token='+localStorage.getItem("token");
    filepath = "file://home/hajer/Downloads/profile.png"
    options = {
        fileKey: "myImage",
        httpMethod: "POST",
        mimeType: "image/jpeg",
        params: {name: "My CV"},
        chunkedMode: true
    }
    images: Array<string>;

    uploading: boolean = true;
    current: number = 1;
    total: number;
    progress: number;
    filesToUpload: Array<File>;


    profile:  ProfilePage = new ProfilePage(this.nav,this.navParams,this.viewCtrl,this.http,this.modalCtrl,this.Alert,this.app);
  constructor(
      public nav: NavController,
      private http:Http, public modalCtrl: ModalController,
      public viewCtrl: ViewController,
      private navParams: NavParams,private app: App,
      private ngZone: NgZone,public Alert: AlertController
  ) {
      this.http=http;
      this.filesToUpload = [];
      this.tab = this.nav.parent;

      }

    upload() {
        // let
        let url = 'https://api.denrole.com/api/profile/add_attachment?token='+localStorage.getItem("token");
        this.makeFileRequest(url, [], this.filesToUpload).then((result) => {
            console.log(result);
            // this.profile;
            this.profile.ionViewWillEnter();
            this.dismiss();
            this.nav.push(TabsPage,{index: "1"});




        }, (error) => {
            console.error(error);
        });
    }
    fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>> fileInput.target.files;
    }
    public dismiss() {
        this.viewCtrl.dismiss();
    }

    makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
        return new Promise((resolve, reject) => {
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();
            for(let i = 0; i < files.length; i++) {
                formData.append("file", files[i]);
                formData.append("name", this.att.name);
                formData.append("type", this.att.type);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            console.log(formData)
            xhr.send(formData);


        });
    }


}
