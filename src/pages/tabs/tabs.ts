import { Component } from '@angular/core';
import {NavController, NavParams, ViewController, AlertController,} from 'ionic-angular';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { SearchPage } from '../search/search';
import { ContactPage } from '../contact/contact';
import {CommunicationPage } from '../communication/communication'
import {LoginPage} from "../login/login";
import { App } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = ProfilePage;
  tab3Root: any = ContactPage;
  tab4Root: any = CommunicationPage;
  tab5Root: any = SearchPage;



  constructor(public navParams: NavParams, public nav: NavController,private viewCtrl: ViewController,private app: App,private alertCtrl: AlertController) {

  }
  index = this.navParams.get('index');
  powerOff(){
    let alert = this.alertCtrl.create({
      title: 'Confirm logout',
      message: 'Do you want to logout',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'logout',
          handler: () => {
            console.log('logout clicked');
            localStorage.removeItem('token');

             this.app.getRootNav().push(LoginPage);
          }
        }
      ]
    });
    alert.present();

    /*var txt;
    var r = confirm("Are you sure?");
    if (r == true) {
      localStorage.removeItem('token');
      // this.nav.setRoot(LoginPage)
      // this.nav.push(LoginPage).then(() => {
      //   // first we find the index of the current view controller:
      //   const index = this.viewCtrl.index;
      //   // then we remove it from the navigation stack
      //   this.nav.remove(index);
      // });
      // this.app.getRootNav().push(LoginPage);
    }*/

  }
}
