import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Headers,Http} from '@angular/http';
import {ContactPage} from "../contact/contact";

/*
  Generated class for the SendMessage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-send-message',
  templateUrl: 'send-message.html'
})
export class SendMessagePage {
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  msg;
  constructor(private http:Http,public navCtrl: NavController, public navParams: NavParams) {
    this.http=http;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SendMessagePage');
  }
  strip_tags(input, allowed) {


    allowed = (((allowed || '') + '')
      .toLowerCase()
      .match(/<[a-z][a-z0-9]*>/g) || [])
      .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
      commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
      .replace(tags, function($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
      });
  }

  onSendMessage(){
    console.log(localStorage.getItem("token"))
    let id = this.navParams.get("contact_id");
    console.log(id);
    let response;
    let mmm =this.msg

    mmm = mmm.replace(/\r?\n/g, '<br />');
    mmm = this.strip_tags(mmm,'<br>')
    if (this.msg) {
      let body = {
        "body" : mmm
      }

      console.log(this.url + 'messages/contact/' +id+ '/send?token=' + localStorage.getItem("token"))
      this.http.post(this.url + 'messages/contact/' +id+ '/send?token=' + localStorage.getItem("token"), body, this.headers).map(res => res.json()).subscribe(
          data => {
            response = data;
            console.log(response);
            if (data.code == 74) {

              this.msg='';
              this.navCtrl.push(ContactPage);
            }
            else {

            }
          },
          error => {
            console.log("ERROR: ", error)
          }
      );
    }
  }
}
