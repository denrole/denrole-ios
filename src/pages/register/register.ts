import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import { NavController, AlertController , NavParams, ViewController} from 'ionic-angular';
import {Headers,Http} from '@angular/http';
import {ConfirmationPage} from "../confirmation/confirmation";
import { PrivacyPage } from '../privacy/privacy';
import { TermsPage } from '../terms/terms';
import { CookiePage } from '../cookie/cookie';

@Component({
  selector: 'page-about',
  templateUrl: 'register.html'
})
export class RegisterPage {
  disable = false;
  user = {
    username : "",
    email: "",
    first_name: "",
    last_name: "",
    password : "",
    location:"",
    profession:""
  }

    roleArray= [];
    workHArray= [];
    typeArray= [];
    headers = new Headers({ 'Content-Type': 'application/json' });
    url = 'https://api.denrole.com/api/';

    constructor(public nav: NavController,private http:Http,public alertCtrl: AlertController,private viewCtrl: ViewController, public navParams: NavParams) {
    this.http = http;
  }
    ionViewWillEnter(){
        this.roleArray=this.navParams.get("roleArray");
        this.workHArray=this.navParams.get("workHArray");
        this.typeArray=this.navParams.get("typeArray");
        console.log(this.roleArray);
        if (this.navParams.get("user")){
            this.user = this.navParams.get("user");
        }
        console.log( this.navParams.get("user"));
    }
  onRegister(){
      this.disable =true
    let body = this.user;
    console.log(body);
    let error;
    let errorMsg;
    this.http.post(this.url+'register', body, this.headers).map(res => res.json()).subscribe(
        data => {
          // response = data;
          console.log(data);
          if (data.code == 74){
              let alert = this.alertCtrl.create({
                  title: 'Success',
                  subTitle: data.message,
                  buttons: ['OK']
              });
              alert.present();
            this.nav.push(ConfirmationPage,{
                registerEmail: this.user.email
            });
          }
          else {
              error = data.data;
              if (error.first_name){
                  errorMsg = error.first_name;
              }
              else if (error.last_name){
                  errorMsg = error.last_name;
              }
              else if (error.username){
                  errorMsg = error.username;
              }
              else if (error.location){
                  errorMsg = error.location;
              }
              else if (error.profession){
                  errorMsg = error.profession;
              }
              else if (error.email){
                  errorMsg = error.email;
              }
              else if (error.password){
                  errorMsg = error.password;
              }
              let alert = this.alertCtrl.create({
                  title: 'Alert',
                  subTitle: errorMsg,
                  buttons: ['OK']
              });
              alert.present();
              this.user.password="";
            this.disable =false
            /*   this.nav.push(RegisterPage, {
                   user: this.user,
                   roleArray: this.roleArray,
                   workHArray: this.workHArray,
                   typeArray: this.typeArray
               }).then(() => {
                   // first we find the index of the current view controller:
                   const index = this.viewCtrl.index;
                   // then we remove it from the navigation stack
                   this.nav.remove(index);
               });*/
          }
        },
        error => {
          this.disable =false
          console.log("ERROR: ", error);
            let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: error.body,
                buttons: ['OK']
            });
            alert.present();
        }
    );

    // console.log(this.http.post(url, body, headers).map(res => res.json()));

  }
    openTermsPage(){
        this.nav.push(TermsPage);
    }
    openPrivacyPage(){
        this.nav.push(PrivacyPage);
    }
    openCookiePage(){
        this.nav.push(CookiePage);
    }
}
