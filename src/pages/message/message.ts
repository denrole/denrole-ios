import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Headers,Http} from '@angular/http';

/*
  Generated class for the Message page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-message',
  templateUrl: 'message.html'
})
export class MessagePage {
  message:Array<Object>;
  sender:'';
  sender_id:'';
  message_id:'';
  last_message_sender;
  last_message_body;
  last_message_picture;
  msg;

  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  constructor(private http:Http,public navCtrl: NavController, public navParams: NavParams) {
    this.http=http;
    this.message_id = navParams.get("message_id");
  }


  ionViewWillEnter() {
    let response;
    let messagearray=[];
    this.http.get(this.url+'messages/'+this.message_id+'/show?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            this.message_id= data.data.message.id
            if (data.data.message.sender.id ==localStorage.getItem("id")){
                this.sender=data.data.message.receiver.username;
                this.sender_id = data.data.message.receiver.id;
            }
            else{
              this.sender=data.data.message.sender.username;
              this.sender_id = data.data.message.sender.id;
            }
            this.last_message_picture = data.data.message.sender.profile_picture;
            this.last_message_sender = data.data.message.sender.username;
            console.log(this.last_message_picture)
            if(data.data.replies) {
              for(var i = 0; i < data.data.replies.length; i++) {
                messagearray[i]=( data.data.replies[i]);

              }
              this.message=messagearray;
              console.log(this.message);
            }
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );
  }
  onSendMessage(id){
    let response;

    if (this.msg) {
      let mmm =this.msg

      mmm = mmm.replace(/\r?\n/g, '<br />');
      mmm = this.strip_tags(mmm,'<br>')
      let body = {
        "body" : mmm
      }
      this.http.post(this.url + 'messages/' +id+ '/send?token=' + localStorage.getItem("token"), body, this.headers).map(res => res.json()).subscribe(
          data => {
            response = data;
            console.log(response);
            if (data.code == 74) {
              this.ionViewWillEnter()
              this.msg='';
            }
            else {

            }
          },
          error => {
            console.log("ERROR: ", error)
          }
      );
    }
  }

  strip_tags(input, allowed) {
    allowed = (((allowed || '') + '')
      .toLowerCase()
      .match(/<[a-z][a-z0-9]*>/g) || [])
      .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
      commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
      .replace(tags, function($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
      });
  }
}
