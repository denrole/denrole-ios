import { Component } from '@angular/core';
import {Headers,Http} from '@angular/http';
import { NavController , NavParams,AlertController} from 'ionic-angular';
import {MeetingPage} from "../meeting/meeting";


@Component({
  selector: 'page-profile-contact',
  templateUrl: 'profile.html'
})
export class ProfileContactPage {

    row ;
    user={
    username: '',
    token: localStorage.getItem("token"),
    profile_picture:'',
    is_published: 0
  };
  attachments:Array<Object>;
  is_friend;
  people: Array<Object>;
    headers = new Headers({ 'Content-Type': 'application/json' });
    url = 'https://api.denrole.com/api/';
  constructor(public nav: NavController,private http:Http, public navParams: NavParams,
              public alertCtrl: AlertController) {


    let body = this.user;
    let response;
    console.log(body);
      let varArray =[] ;
      this.row = [];
    let idotheruser= localStorage.getItem("idOtherUser")
    this.http.get(this.url+'users/'+idotheruser+'/profile?token='+this.user.token, this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){

              this.user.username = data.data.user.username;
              this.user.profile_picture = 'https://api.denrole.com/uploads/profile_pictures/'+data.data.user.profile_picture;
              this.user.is_published = data.data.user.is_published;
              this.is_friend= data.data.is_friend;
              this.attachments = data.data.attachments;
              console.log(this.is_friend)
              // this.user.username = data.data.user.username;
              for (let i = 0; i < this.attachments.length; ){

                  for(let j =0 ; j < 3 && i < this.attachments.length; j ++){
                      varArray.push(this.attachments[i]);
                      i++;
                  }
                  this.row.push(varArray);
                  varArray = []
              }
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );

  }
    AddFriend(id){
        console.log("add friend");
        let response;
        this.http.get(this.url+'contacts/'+id+'/send_request?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
            data => {
                response = data;
                console.log(response);
                if (data.code == 74){
                    let alert = this.alertCtrl.create({
                        title: 'Success',
                        subTitle: data.message,
                        buttons: ['OK']
                    });
                    alert.present();
                }
                else {
                    let alert = this.alertCtrl.create({
                        title: 'Alert',
                        subTitle: "Invitation already send to this user",
                        buttons: ['OK']
                    });
                    alert.present();
                }
            },
            error => {
                console.log("ERROR: ", error);
                let alert = this.alertCtrl.create({
                    title: 'Alert',
                    subTitle: "Invitation already send to this user",
                    buttons: ['OK']
                });
                alert.present();
            }
        );
    }
    requestMeeting(){
        let id = localStorage.getItem('idOtherUser');
        this.nav.push(MeetingPage,{
            idContact : id
        })
    }
}
