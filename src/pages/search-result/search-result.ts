import { Component } from '@angular/core';
import {Headers,Http} from '@angular/http';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ProfileContactPage } from '../profileContact/profile';
import {ProfilePage} from "../profile/profile";
/*
  Generated class for the SearchResult page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-search-result',
  templateUrl: 'search-result.html'
})
export class SearchResultPage {
  searchItem;
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  posts;
  user= localStorage.getItem("id")
  constructor(private http:Http,public nav: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
    this.navParams = navParams;
    this.http=http;
  }

  ionViewWillEnter() {
    this.searchItem=this.navParams.get("searchItem");
    console.log(this.searchItem)
    let response;
    this.http.post(this.url+'posts/search?token='+localStorage.getItem("token"),this.searchItem, this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
           this.posts=data.data.posts;
           console.log(this.posts)
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );
  }
  OnClickAvatar(id,boolean){
      console.log(id)
      console.log(boolean)
    if (id == this.user){
      this.nav.push(ProfilePage);
    } else {
        localStorage.setItem("idOtherUser", id);
        localStorage.setItem("isfriend", boolean);
        this.nav.push(ProfileContactPage);
    }

  }
  AddFriend(id){
    let response;
    this.http.get(this.url+'contacts/'+id+'/send_request?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
              let alert = this.alertCtrl.create({
                  title: 'Alert',
                  subTitle: data.message,
                  buttons: ['OK']
              });
              alert.present();
            this.ionViewWillEnter()
          }
          else {
          }
        },
        error => {
          console.log("ERROR: ", error)
            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: "Invitation already sent to this user",
                buttons: ['OK']
            });
            alert.present();
        }
    );
  }

}
