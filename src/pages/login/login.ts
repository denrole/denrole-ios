import { Component } from '@angular/core';

import {NavController, ViewController, AlertController, NavParams} from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { TabsPage } from '../tabs/tabs';
import { PrivacyPage } from '../privacy/privacy';
import { TermsPage } from '../terms/terms';
import { CookiePage } from '../cookie/cookie';
import {ResetPasswordPage} from "../reset-password/reset-password";

import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {ConfirmationPage} from "../confirmation/confirmation";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  disable = this.navParams.get('deactivate')
  disableL = false;
  disableR = false;
  user={
    username: "",
    password: ""
  };
  token;
  roleArray=[];
  workHArray=[];
  typeArray=[];
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';

  public itemList: Array<Object>;
  constructor(public nav: NavController,private http:Http,private viewCtrl: ViewController,
              public alertCtrl: AlertController,public navParams: NavParams,) {
    this.http = http;
    this.itemList = [];

  }

  ionViewWillEnter() {
    if (this.navParams.get('deactivate')){
      this.disableL = true;
      this.disableR = true;
      console.log(this.disableL)
      console.log(this.disableR)

    }
  }

  openRegisterPage() {
    this.disableR = true;
    let response;
    this.http.get(this.url+'posts/form_elemens?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            if(data.data.roles){
              this.roleArray = data.data.roles;
            }
            if (data.data.work_hours){
              this.workHArray = data.data.work_hours;
            }
            if (data.data.types){
              this.typeArray= data.data.types;
            }

            this.disableR = false;
            this.nav.push(RegisterPage,{
              roleArray: this.roleArray,
              workHArray: this.workHArray,
              typeArray: this.typeArray
            });
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );

  }
  onLogin(){

    this.disableL =true;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let url = 'https://api.denrole.com/api/login';
    let body = this.user;
    let response;
    console.log(body);
    this.http.post(url, body, headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            console.log("before add")
            localStorage.setItem("token", data.data.token);
            localStorage.setItem("id", data.data.user.id);
            this.nav.push(TabsPage).then(() => {
              // first we find the index of the current view controller:
              const index = this.viewCtrl.index;
              // then we remove it from the navigation stack
              this.nav.remove(index);
            });
          }else{
            this.disableL = false;
                let alert = this.alertCtrl.create({
                  title: 'Alert',
                  subTitle: data.message,
                  buttons: ['OK']
                });
                alert.present();
                if (data.message !== "These credentials do not match our records.")
                this.nav.push(ConfirmationPage);
              }



        },
        error => {
          this.disableL = false;
          console.log("ERROR: ", error);
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: "Wrong password or username!",
            buttons: ['OK']
          });
          alert.present();
        }
    );


  }
  openTermsPage(){
    this.nav.push(TermsPage);
  }
  openPrivacyPage(){
    this.nav.push(PrivacyPage);
  }
  openCookiePage(){
    this.nav.push(CookiePage);
  }
  onloginTest(){
    this.nav.push(TabsPage);
  }
  onResetPwd(){
    this.nav.push(ResetPasswordPage);
  }
}
