import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import {Headers,Http} from '@angular/http';
import {ProfilePage} from "../profile/profile";

/*
  Generated class for the ConfirmDelete page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-confirm-delete',
  templateUrl: 'confirm-delete.html'
})
export class ConfirmDeletePage {

  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';
  user={
    username: '',
    token: localStorage.getItem("token"),
    profile_picture:'',
    is_published: 0
  };
  id ;
  constructor(public navCtrl: NavController,  private http:Http, public navParams: NavParams,public Alert: AlertController) {}

  deleteAttachment(){
    this.id = this.navParams.get('id')
    console.log(this.id)
    let response;
    this.http.get(this.url+'profile/attachments/'+this.id+'/delete?token='+this.user.token, this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            console.log(data.data.user.is_published);

            let alert = this.Alert.create({
              title: 'Success',
              subTitle: data.message,
              buttons: ['OK']
            });
            alert.present();
            this.navCtrl.push(ProfilePage);
          }
          else {
            let alert = this.Alert.create({
              title: 'Alert',
              subTitle: data.message,
              buttons: ['OK']
            });
            alert.present();
            this.navCtrl.push(ProfilePage);
          }
        },
        error => {
          console.log("ERROR: ", error)
          let alert = this.Alert.create({
            title: 'Error',
            subTitle: error,
            buttons: ['OK']
          });
          alert.present();
          this.navCtrl.push(ProfilePage);
        }
    );
  }
  cancelDelete(){
    this.navCtrl.push(ProfilePage);
  }
}
