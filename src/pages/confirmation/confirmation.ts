import { Component } from '@angular/core';
import { NavController, NavParams , AlertController} from 'ionic-angular';
import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {LoginPage} from "../login/login";
import { PrivacyPage } from '../privacy/privacy';
import { TermsPage } from '../terms/terms';
import { CookiePage } from '../cookie/cookie';

/*
  Generated class for the Confirmation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-confirmation',
  templateUrl: 'confirmation.html'
})
export class ConfirmationPage {
  user={
    email: "",
    code:""
  };
  constructor(public nav: NavController, public navParams: NavParams,private http:Http, public alertCtrl: AlertController) {

  }
  ionViewWillEnter(){
    if (this.navParams.get("registerEmail")) {
      this.user.email = this.navParams.get("registerEmail");
    }
  }
  onConfirm(){


    let headers = new Headers({ 'Content-Type': 'application/json' });
    let url = 'https://api.denrole.com/api/register/confirm';


    let body = this.user;
    let response;
    console.log(body);
    this.http.post(url, body, headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            let alert = this.alertCtrl.create({
              title: 'Success',
              subTitle: data.message,
              buttons: ['OK']
            });
            alert.present();
            this.nav.push(LoginPage)

          }
          else {
          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );

  }
  openTermsPage(){
    this.nav.push(TermsPage);
  }
  openPrivacyPage(){
    this.nav.push(PrivacyPage);
  }
  openCookiePage(){
    this.nav.push(CookiePage);
  }
}
