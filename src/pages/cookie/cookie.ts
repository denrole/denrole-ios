import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-cookie',
  templateUrl: 'cookie.html'
})
export class CookiePage {

  constructor(public navCtrl: NavController) {

  }

}
