import { Component } from '@angular/core';
import {Headers,Http} from '@angular/http';
import {NavController, Tabs, NavParams, ViewController, AlertController} from 'ionic-angular';
import { ProfileContactPage } from '../profileContact/profile';
import {SendMessagePage} from "../send-message/send-message";
import {TabsPage} from "../tabs/tabs";
import { App } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  contact:Array<Object>;
  contactRequest:Array<Object>;
  headers = new Headers({ 'Content-Type': 'application/json' });
  url = 'https://api.denrole.com/api/';

    powerOff = new TabsPage( this.navParams,  this.nav,this.viewCtrl, this.app,this.Alert)
  constructor(private http:Http, public nav: NavController,public navParams: NavParams,private viewCtrl: ViewController,private app: App,public Alert: AlertController) {
    this.http=http;

  }
  ionViewWillEnter() {
    let response;
    this.http.get(this.url+'contacts?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
            console.log(data)
            this.contact=data.data.contacts;
            this.contactRequest=data.data.contactRequests;
            console.log(this.contactRequest)

          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );
  }
  AcceptInvitation(id){
    let response;
      let t: Tabs = this.nav.parent;
    this.http.get(this.url+'contacts/'+id+'/accept_request?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){
              this.ionViewWillEnter()
              t.select(2);
          }
          else {
          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );
  }
  DenyInvitation(id){
    let response;
      let t: Tabs = this.nav.parent;
    this.http.get(this.url+'contacts/'+id+'/deny_request?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
        data => {
          response = data;
          console.log(response);
          if (data.code == 74){

              this.ionViewWillEnter()
              t.select(2);
          }
          else {

          }
        },
        error => {
          console.log("ERROR: ", error)
        }
    );
  }

  DeleteContact(id){
    let response;
      let t: Tabs = this.nav.parent;
      let r = confirm("Please Confirm");
      if (r == true) {
          this.http.get(this.url+'contacts/'+id+'/remove?token='+localStorage.getItem("token"), this.headers).map(res => res.json()).subscribe(
              data => {
                  response = data;
                  console.log(response);
                  if (data.code == 74){

                      this.ionViewWillEnter()
                      t.select(2);
                  }
                  else {

                  }
              },
              error => {
                  console.log("ERROR: ", error)
              }
          );
      }

  }
  public OnClickAvatar(id,boolean){
      this.nav.push(ProfileContactPage,{
          contact_id: id,
          is_friend: boolean
      })

  }
  SendMessage(id){
      this.nav.push(SendMessagePage,{
          contact_id: id
      })
  }



}
