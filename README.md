			
## Start Denrole Project

1- Clone it

```
git clone https://hajerhleli@bitbucket.org/denrole/denrole-ios.git

```

2- Install all dependencies

```
$ npm install

```

3- Run it in browser 

```
$ npm run ionic:serve

```


### Build IOS ###


*IOS

```
$ ionic cordova platform add ios
$ ionic cordova build ios

```
